#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp" // For HoG.
#include "opencv2/features2d.hpp"
#include "opencv2/opencv.hpp"
//#include "baseapi.h"
//#include <allheaders.h>
//#include "gui.hpp"


#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

using namespace cv;
using namespace std;

RNG rng(12345);
int thresh = 100;

HOGDescriptor hog;

vector<Point> contoursConvexHull( vector<vector<Point> > contours )
{
    vector<Point> result;
    vector<Point> pts;
    for ( size_t i = 0; i< contours.size(); i++)
        for ( size_t j = 0; j< contours[i].size(); j++)
            pts.push_back(contours[i][j]);
    convexHull( pts, result );
    return result;
}

//Remove the boundary whitespaces
void removeBoundry(Mat& in){
    
    //cv::Mat in = cv::imread("CropWhite.jpg");
    
    // vector with all non-black point positions
    std::vector<cv::Point> nonBlackList;
    nonBlackList.reserve(in.rows*in.cols);
    
    // add all non-black points to the vector
    //TODO: there are more efficient ways to iterate through the image
    for(int j=0; j<in.rows; ++j)
        for(int i=0; i<in.cols; ++i)
        {
            // if not black: add to the list
            if(in.at<cv::Vec3b>(j,i) == cv::Vec3b(0,0,0))
            {
                nonBlackList.push_back(cv::Point(i,j));
            }
        }
    
    // create bounding rect around those points
    cv::Rect bb = cv::boundingRect(nonBlackList);
    
    // display result and save it
    //cv::imshow("found rect", in(bb));
    in = in(bb);
    //cv::imwrite("CropWhiteResult.png", in(bb));
    
}

//set upper and lower level staff line position above 1 = 0 on 1=1 on 2=2,..below 5 = 6
void setPitch(int upper, int lower, vector<int> &upper_side, vector<int> &lower_side, vector<int> staff_positions, vector<int> con_end_positions, int x)
{
    printf("\nupper %d\n",upper);
    printf("lower %d\n",lower);

//    if(upper + x < staff_positions[0]){
//        upper_side.push_back(0);
//        printf("pitch %d ",0);
//    }
//
//    if(upper < staff_positions[0] && upper > staff_positions[0] -x){
//        upper_side.push_back(1);
//        printf("pitch %d ",1);
//    }
//
//    if(upper > staff_positions[0] && upper < staff_positions[0] +x){
//        upper_side.push_back(2);
//        printf("pitch %d ",2);
//    }
//
//    if(upper < staff_positions[con_end_positions[1]] && upper > staff_positions[con_end_positions[1]] -x){
//        upper_side.push_back(3);
//        printf("pitch %d ",3);
//    }
//
//    if(upper > staff_positions[con_end_positions[1]] && upper < staff_positions[con_end_positions[1]] +x){
//        upper_side.push_back(4);
//        printf("pitch %d ",4);
//    }
//
//    if(upper < staff_positions[con_end_positions[2]] && upper > staff_positions[con_end_positions[2]] -x){
//        upper_side.push_back(5);
//        printf("pitch %d ",5);
//    }
//
//    if(upper > staff_positions[con_end_positions[2]] && upper < staff_positions[con_end_positions[2]] +x){
//        upper_side.push_back(6);
//        printf("pitch %d ",6);
//    }
//
//    if(upper < staff_positions[con_end_positions[3]] && upper > staff_positions[con_end_positions[3]] -x){
//        upper_side.push_back(7);
//        printf("pitch %d ",7);
//    }
//
//    if(upper > staff_positions[con_end_positions[3]] && upper < staff_positions[con_end_positions[3]] +x){
//        upper_side.push_back(8);
//        printf("pitch %d ",8);
//    }
//
//    if(upper < staff_positions[con_end_positions[4]]+x && upper > staff_positions[con_end_positions[4]] -x){
//        upper_side.push_back(9);
//        printf("pitch %d ",9);
//    }
//
//    if(upper > staff_positions[con_end_positions[4]]+4){
//        upper_side.push_back(10);
//        printf("pitch %d ",10);
//    }
    
    
    
    
    //Upper level staff lines
    if(upper < staff_positions[0]){
        upper_side.push_back(0);
        printf("pitch %d ",0);
    } else if(upper > staff_positions[con_end_positions[4]]){
        upper_side.push_back(10);
        printf("pitch %d ",10);
    } else {
        //On the lines
        for (int i=0; i<=con_end_positions[0]; i++) {
            if(staff_positions[i]==upper){
                upper_side.push_back(1);
                printf("pitch %d ",1);
            }
        }
        for (int i=con_end_positions[0]; i<=con_end_positions[1]; i++) {
            if(staff_positions[i]==upper){
                upper_side.push_back(3);
                printf("pitch %d ",3);
            }
        }
        for (int i=con_end_positions[1]; i<=con_end_positions[2]; i++) {
            if(staff_positions[i]==upper){
                upper_side.push_back(5);
                printf("pitch %d ",5);
            }
        }
        for (int i=con_end_positions[2]; i<=con_end_positions[3]; i++) {
            if(staff_positions[i]==upper){
                upper_side.push_back(7);
                printf("pitch %d ",7);
            }
        }
        for (int i=con_end_positions[3]; i<=con_end_positions[4]; i++) {
            if(staff_positions[i]==upper){
                upper_side.push_back(9);
                printf("pitch %d ",9);
            }
        }
        //In between lines
        for (int i=staff_positions[con_end_positions[0]]+1; i<staff_positions[con_end_positions[0]+1]; i++) {
            if(i==upper){
                upper_side.push_back(2);
                printf("pitch %d ",2);
            }
        }
        for (int i=staff_positions[con_end_positions[1]]+1; i<staff_positions[con_end_positions[1]+1]; i++) {
            if(i==upper){
                upper_side.push_back(4);
                printf("pitch %d ",4);
            }
        }
        for (int i=staff_positions[con_end_positions[2]]+1; i<staff_positions[con_end_positions[2]+1]; i++) {
            if(i==upper){
                upper_side.push_back(6);
                printf("pitch %d ",6);
            }
        }
        for (int i=staff_positions[con_end_positions[3]]+1; i<staff_positions[con_end_positions[3]+1]; i++) {
            if(i==upper){
                upper_side.push_back(8);
                printf("pitch %d ",8);
            }
        }
}
    
    //Lower level staff lines
    if(lower > staff_positions[con_end_positions[4]]){
        lower_side.push_back(10);
        printf("%d\n",10);
    } else if(lower < staff_positions[0]){
        lower_side.push_back(0);
        printf("%d\n",0);
    } else {
        //On the lines
        for (int i=0; i<=con_end_positions[0]; i++) {
            if(staff_positions[i]==lower){
                lower_side.push_back(1);
                printf("%d\n",1);
            }
        }
        for (int i=con_end_positions[0]; i<=con_end_positions[1]; i++) {
            if(staff_positions[i]==lower){
                lower_side.push_back(3);
                printf("%d\n",3);
            }
        }
        for (int i=con_end_positions[1]; i<=con_end_positions[2]; i++) {
            if(staff_positions[i]==lower){
                lower_side.push_back(5);
                printf("%d\n",5);
            }
        }
        for (int i=con_end_positions[2]; i<=con_end_positions[3]; i++) {
            if(staff_positions[i]==lower){
                lower_side.push_back(7);
                printf("%d\n",7);
            }
        }
        for (int i=con_end_positions[3]; i<=con_end_positions[4]; i++) {
            if(staff_positions[i]==lower){
                lower_side.push_back(9);
                printf("%d\n",9);
            }
        }
        //In between lines
        for (int i=staff_positions[con_end_positions[0]]+1; i<staff_positions[con_end_positions[0]+1]; i++) {
            if(i==lower){
                lower_side.push_back(2);
                printf("%d\n",2);
            }
        }
        for (int i=staff_positions[con_end_positions[1]]+1; i<staff_positions[con_end_positions[1]+1]; i++) {
            if(i==lower){
                lower_side.push_back(4);
                printf("%d\n",4);
            }
        }
        for (int i=staff_positions[con_end_positions[2]]+1; i<staff_positions[con_end_positions[2]+1]; i++) {
            if(i==lower){
                lower_side.push_back(6);
                printf("%d\n",6);
            }
        }
        for (int i=staff_positions[con_end_positions[3]]+1; i<staff_positions[con_end_positions[3]+1]; i++) {
            if(i==lower){
                lower_side.push_back(8);
                printf("%d\n",8);
            }
        }
    }

}

//Place consecutive groups end position in different vector
vector<int> conseccount(const vector<int> &v)
{
    size_t s = v.size();
    int ret = 0;
    int c_count = 0;
    //bool seqstart = true;
    vector<int> c_end_position;
    vector<int> consecutive_counts;
    //vector<int> line_thick;
 
    for(size_t i = 1; i < s; i++)
    {
        if(v[i - 1] + 1 == v[i])
        {
            ret++;
            c_count++;
        }
        else {
            //consecutive_counts.push_back(c_count);
            //line_thick.push_back(c_count);
            c_end_position.push_back(ret);
            ret++;
            c_count=0;
        }
    }
    c_end_position.push_back(ret);
    
    for (int i=0; i<c_end_position.size(); i++) {
        cout << "CONSECUTIVE POINT :  "<< c_end_position.at(i) << endl;
    }
    
    //TODO: Find average of line_thick size objects
//    for (int i=0; i<line_thick.size(); i++) {
//        cout << "CONSECUTIVE POINT :  "<< c_end_position.at(i) << endl;
//    }
    
    //Get first maximum consecutive points
    for (int i=0; i<5; i++) {
        //cout << "CONSECUTIVE POINTs counted :  "<< consecutive_counts.at(i) << endl;
        consecutive_counts.push_back(c_end_position[i]);
        cout << "CONSECUTIVE POINT REMOVED LAST :  "<< consecutive_counts.at(i) << endl;
    }
    
    //return c_end_position;
    return consecutive_counts;
}

vector<Rect> get_SortedBoxes(vector<Rect>& boundRect)
{
    vector<Rect> temp;
    
    for(int i = 0; i<boundRect.size()-1; i++) {
        for(int j = i; j<boundRect.size(); j++) {
            if(boundRect[i].x > boundRect[j].x){
                temp.push_back(boundRect[i]);
                boundRect[i]=boundRect[j];
                boundRect[j]=temp.at(0);
                temp.clear();
            }
        }
    }

    return boundRect;
}

void help()
{
    cout << "\nThis program reads and plays sheet music from a scanned image.\n"
    << "Usage:\n"
    << "./sightread <image_name>, Default is test.jpg\n"
    << endl;
}

float median(vector<float> &v)
{
    size_t n = v.size() / 2;
    nth_element(v.begin(), v.begin()+n, v.end());
    return v[n];
}

void rotate(cv::Mat& src, double angle, cv::Mat& dst)
{
    cv::Point2f pt(src.cols/2.0, src.rows/2.0);
    cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
    cv::Rect bbox = cv::RotatedRect(pt, src.size(), angle).boundingRect();
    r.at<double>(0,2) += bbox.width/2.0 - pt.x;
    r.at<double>(1,2) += bbox.height/2.0 - pt.y;
    
    cv::warpAffine(src, dst, r, bbox.size(), cv::INTER_LANCZOS4, cv::BORDER_CONSTANT, Scalar(255, 255, 255));
}

// Input: An edge detected image.
double find_rotation_angle(cv::Mat& edges, Mat& c_src)
{
    vector<Vec4i> lines;
    vector<float> slopes;
    
    HoughLinesP(edges, lines, 1, CV_PI/180, 100, 50, 10 );

    //lines.size gives total number of staff lines(horizontal lines)
    if(lines.size() == 0) {
        std::cout << "Not enough lines in this image!" << std::endl;
        exit(-1);
    }
    
    std::cout << "Lines size(no of hough lines) : "<< lines.size() << std::endl;
    
    for( size_t i = 0; i < lines.size(); i++ ) {
        Vec4i l = lines[i];
        line(c_src , Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
        slopes.push_back((abs(l[0] - l[2]) < 0.0000001) ? 1000000 : (l[1]-l[3]) / (float)(l[0]-l[2]));
    }
    
    double rotation = atan(median(slopes)) * 180.0/CV_PI;
    
    return rotation;
}

void vert_projection(Mat& img, vector<int>& histo)
{
    Mat proj = Mat::zeros(img.rows, img.cols, CV_8UC1);
    int count, i, j, k;
    
    for(i=0; i < img.cols; i++) {
        for(count = 0, j=0; j < img.rows; j++) {
            for(k=0; k < img.channels(); k++) {
                count += (img.at<int>(j, i, k)) ? 0:1;      //j is yaxis, i is x axis
            }
        }
        
        histo.push_back(count);
        line(proj, Point(i, 0), Point(i, count/img.channels()), Scalar(255), 2, 0, 0);
    }
    
    imshow("proj vertical", proj);
}

void horiz_projection(Mat& img, vector<int>& histo)
{
    Mat proj = Mat::zeros(img.rows, img.cols, CV_8UC1);
    int count, i, j;
    
    for(i=0; i < img.rows; i++) {
        for(count = 0, j=0; j < img.cols; j++) {
            count += (img.at<int>(i, j)) ? 0:1;
        }
        
        histo.push_back(count);
        line(proj, Point(0, i), Point(count, i), Scalar(255), 2, 0, 0);
        //line(img, Point(0, i), Point(count, i), Scalar(0,0,0), 1, 4);
    }
    
    imshow("proj horizontal", proj);
}

void flood_line(Mat& img, Point seed, vector<Point>& line_pts)
{
    // If point isn't black, return.
    if( img.at<uchar>(seed) != 0 ) {
        return;
    }
    if( seed.x < 0 || seed.x > img.rows || seed.y < 0 || seed.y > img.cols );
    if( std::find(line_pts.begin(), line_pts.end(), seed)!=line_pts.end() ) { return; }
    
    line_pts.push_back(seed);
    
    img.at<uchar>(seed) = 255;
    flood_line(img, Point(seed.x - 1, seed.y), line_pts);
    flood_line(img, Point(seed.x + 1, seed.y), line_pts);
    flood_line(img, Point(seed.x, seed.y - 1), line_pts);
    flood_line(img, Point(seed.x, seed.y + 1), line_pts);
}

void remove_staff2(Mat& orig, Mat& img, int index)
{
    char b = rng.uniform(0, 255);
    char g = rng.uniform(0, 255);
    char r = rng.uniform(0, 255);
    
    for(int x = 0; x < img.cols; x++) {
        if(img.at<uchar>(index, x) == 0) {      // if y=index, x=0,1,2.. is black then floodline
            vector<Point> pts;
            flood_line(img, Point(x, index), pts);
            for( vector<Point>::iterator it = pts.begin(); it != pts.end(); ++it) {
                orig.at<Vec3b>(it->y, it->x)[0] = b;
                orig.at<Vec3b>(it->y, it->x)[1] = g;
                orig.at<Vec3b>(it->y, it->x)[2] = r;
            }
        }
    }
}

void remove_staff(Mat& img, int index)
{
    for(int x = 0; x < img.cols; x++) {
        //printf("pixel value at (y_%d,x_%d) - %d\n", index, x, img.at<uchar>(index, x));
        //example index is considered 33
        if(img.at<uchar>(index, x) == 0) {      // if the point is black (y_33, x_0 is 0(means blck)
            int sum = 0;
            
            for(int y = -3; y <= 3; y++) {      //Adjust the removing pixels if lines are thick
                if(index + y > 0 && index + y < img.rows) {     //if in between image start and image end (to handle exception)
                    sum += img.at<uchar>(index+y, x);
                }
            }
            //if more black pixels are in between 30 - 36, then replace 31-35 by white pixels
            if(sum >1000) {
                for(int y = -2; y <= 2; y++) {
                    if(index + y > 0 && index + y < img.rows) {  //if in between image start and image end (to handle exception)
                        img.at<uchar>(index+y, x) = 255;
                        //vector<Point> pts;
                        //flood_line(img, Point(x, index), pts);
                    }
                }
            }
        }
    }
}


void printExternalContours(cv::Mat img, vector<vector<Point>> const& contours, vector<Vec4i> const& hierarchy, int const idx)
{
    //for every contour of the same hierarchy level
    for(int i = idx; i >= 0; i = hierarchy[i][0])
    {
        //print it
        cv::drawContours(img, contours, i, cv::Scalar(255));
        
        //for every of its internal contours
        for(int j = hierarchy[i][2]; j >= 0; j = hierarchy[j][0])
        {
            //recursively print the external contours of its children
            printExternalContours(img, contours, hierarchy, hierarchy[j][2]);
        }
    }
}
void find_contours(int t, Mat& threshold_output, vector<Rect>& boundRect, vector<int>& v_p)
{
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    
    // Find contours
    findContours( threshold_output, contours, hierarchy, RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
    
    //-------------REMOVING THE INSIDE CONTOURS---------------------------------------------------------------------------
    
    cout << "IMAGE Width : " << threshold_output.cols << endl;
    cout << "IMAGE Height: " << threshold_output.rows << endl;
    
    //Draw the contours
    Mat dr = Mat::zeros( threshold_output.size(), CV_8UC3 );
    for( size_t i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours( dr, contours, (int)i, color, 2, 8, hierarchy, 0, Point() );
    }
    imshow( "Contours", dr );
    imwrite( "/Users/allen/IOSProjects/MusicPal/Contours.jpg", dr );
    

    //Find Maximum contour (Always remove the contour of whole image)
    int max = 0;
    int position = 0;
    for (int i=0; i<contours.size(); i++) {
        std::cout << i << " - " << contourArea(contours[i]) << '\n';
        if(contourArea(contours[i]) > max) {
            max = contourArea(contours[i]);
            position = i;
        }
    }
    std::cout << "Maximum Area : " << max << '\n';
    contours.erase(contours.begin() + position);
    
    printExternalContours(threshold_output, contours, hierarchy, 0);
    
    waitKey();
    
    //Calculate the mean area of the contours
        long sum=0;
        float average = 0;
    
        for (unsigned int i = 0;  i < contours.size();  i++)
        {
            std::cout << "# of contour points: " << contours[i].size() << std::endl;
    
            for (unsigned int j=0;  j<contours[i].size();  j++)
            {
                std::cout << "Point(x,y)=" << contours[i][j] << std::endl;
            }
    
            std::cout << " Area: " << contourArea(contours[i]) << std::endl;
            sum = sum + contourArea(contours[i]);
        }
    
        average = sum/contours.size();
        printf("Average area %f\n",average);
    
    //Remove the unnecessary contours
//    for (int i=0; i<contours.size(); i++) {
//        if(contourArea(contours[i]) < 3) {
//            contours.erase(contours.begin() + i);
//        }
//    }
    
    //show the cropped image from vertical projection
        for (int i = 0;  i <= v_p.size();  i++)
        {
            if(v_p.size()==i){

            } else{
                cv::Mat image(threshold_output);
                int width = v_p[i+1]-v_p[i];
                if (width>0 && width<threshold_output.cols) {
        
                cv::Rect myROI(v_p[i],0, width, threshold_output.rows);        //(x, y, width, height)
                printf("ROI x %d\n",myROI.x);
                printf("ROI Width %d\n",myROI.width);
                cv::Mat croppedImage = image(myROI);
                //imshow("crop image", croppedImage);

                vector<vector<Point> > cntrs;
                vector<Vec4i> h;

                findContours( croppedImage, cntrs, h, RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
                Mat d = Mat::zeros( croppedImage.size(), CV_8UC3 );
                for( size_t i = 0; i< cntrs.size(); i++ )
                {
                    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
                    drawContours( d, cntrs, (int)i, color, 2, 8, hierarchy, 0, Point() );
                }
                
            
                //imshow( "Cors", d );


                //waitKey();

            }
            }
        }

    Mat dra = Mat::zeros( threshold_output.size(), CV_8UC3 );
    for( size_t i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours( dra, contours, (int)i, color, 2, 8, hierarchy, 0, Point() );
    }

    //Printing the contours values
//    for (auto i = contours.begin(); i != contours.end(); ++i){
//        std::cout << *i << '\n';
//    }
    
    //----------------------------------------------------------------------------------------

    // Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly( contours.size() );
    vector<Point2f>center( contours.size() );
    vector<float>radius( contours.size() );
    boundRect.resize( contours.size() );
    
    //Assign each contours to the bboxes as rectangles
    for( int i = 0; i < contours.size(); i++ ) {
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, false );
        boundRect[i] = boundingRect( Mat(contours_poly[i]) );           // to be imshow on the main method
    }
    
    /// Find the convex hull object for each contour
    vector<vector<Point> >hull( contours.size() );
    for( int i = 0; i < contours.size(); i++ ) {
        convexHull( Mat(contours[i]), hull[i], false );
    }
    
    // Draw polygonal contour + bounding rects + circles + convex hull
    Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
    for( int i = 0; i < contours.size(); i++ ) {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours( drawing, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
        rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );
        //cv::rectangle(drawing, cv::boundingRect(contours[i]), cv::Scalar(255));
        drawContours( drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
    }
    
    
//    vector<Point> ConvexHullPoints =  contoursConvexHull(contours);
//
//    polylines( drawing, ConvexHullPoints, true, Scalar(0,0,255), 2 );
//    imshow( "ContoursAfterDel", dra );
    
    imshow( "RectangleBOX(contours cover)", drawing );
    imwrite( "/Users/allen/IOSProjects/MusicPal/ContoursRect.jpg", drawing );
}

int main(int argc, char** argv)
{
    const char* filename = argc >= 2 ? argv[1] : "test.jpg";
    
    Mat whole_image; //Load whole image
    Mat wh_gsrc; //Grayscale whole source image.
    Mat wh_bw_src; //B&W whole thresholded version of source image.
    Mat wh_edges;      // Wh Edge detected source image.
    
    
    Mat c_src;      // Color source image.
    Mat g_src;      // Grayscale source image.
    Mat bw_src;     // B&W thresholded version of source image.
    Mat edges;      // Edge detected source image.
    
    //--WHOLE IMAGE LOADER---------------------------------------------------------------------------------------------------------

    string w_imageName("/Users/allen/IOSProjects/MusicPal/InputImages/500.png"); // by default 400.png or 344.png 404.png for test (test.jpg)
    if( argc > 1)
    {
        w_imageName = argv[1];
    }
    whole_image = imread(w_imageName.c_str(), IMREAD_COLOR); // Read the file

    if(whole_image.empty()) {
        help();
        cout << "can not open " << filename << endl;
        return -1;
    }
    
    removeBoundry(whole_image);

    
    cvtColor(whole_image, wh_gsrc, CV_RGB2GRAY);
    Canny(wh_gsrc, wh_edges, 50, 200, 3);

    
    //ROTATION STARTS-----------------------------------------------------------------------------------------------
    imshow("Canny edges", wh_edges);
    
    double wh_angle = find_rotation_angle(wh_edges, whole_image);
    //imshow("with rotation cal lines", whole_image);
    
    
    std::cout << "Rotating image by " << abs(wh_angle) << " degrees ";
    if(wh_angle > 0) { std::cout << "counter-"; }
    std::cout << "clockwise."  << std::endl;
    
    // Fix the rotation of the music. Guarantee the staff is horizontal.
    rotate(whole_image, wh_angle, whole_image);
    rotate(wh_gsrc, wh_angle, wh_gsrc);
    
    ///--------------ROTATION CODE ENDED---------------------------------------------------------------------------------
    

    
    vector<int> wh_horiz_proj;
    threshold(wh_gsrc, wh_bw_src, 100, 255, CV_THRESH_OTSU);//|CV_THRESH_BINARY);
    horiz_projection(wh_bw_src, wh_horiz_proj);
    

    vector<int> positions;
    cv::Mat croppedImage;
    
    int sum = 0;
    for (int i=0;i<wh_horiz_proj.size();i++) {
        cout << "index :"<< i <<" value :"<<wh_horiz_proj[i] << endl;
        //Calculating the zero pixel values. or low black pixel count values
                if (wh_horiz_proj[i]<5) {               //default should be 5
                    sum++;
                } else {
                    sum = 0;
                }
                if (sum>30) {                   //default should be 30
                    positions.push_back(i);
                    sum = 0;
                }
    }
    
    //Draw lines and crop stave segments
     int count = 0;
        
    for (int i=0; i<positions.size(); i++) {
        cout << "STAVE SEGMENT(GreenLine) POS and VAL : "<< i <<" : "<<positions[i] << endl;
        line(whole_image, Point(0, positions[i]), Point(whole_image.cols, positions[i]), Scalar( 0, 255, 0), 2, 0, 0);
        
        if (i==0) {
            //Crop first image
            cv::Mat image(wh_gsrc);
            cv::Rect myROI(0,0,wh_gsrc.cols,positions[i]);        //(x, y, width, height)
            croppedImage = image(myROI);
            std::ostringstream w_name;
            w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
            imshow("crop"+std::to_string(count), croppedImage);
            imwrite(w_name.str(), croppedImage);
            count++;
        }
        if (i==positions.size()-1){
            //Crop last image
            cv::Mat image(wh_gsrc);
            cv::Rect myROI(0,positions[i],wh_gsrc.cols,wh_gsrc.rows-positions[i]);        //(x, y, width, height)
            croppedImage = image(myROI);
            std::ostringstream w_name;
            w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
            imshow("crop"+std::to_string(count), croppedImage);
            imwrite(w_name.str(), croppedImage);
            count++;
        } else if (i>0) {
            if(i%2 == 0){
                continue;
            } else{
                //Crop middle images
                cv::Mat image(wh_gsrc);
                cv::Rect myROI(0,positions[i],wh_gsrc.cols,positions[i+1]-positions[i]);        //(x, y, width, height)
                croppedImage = image(myROI);
                std::ostringstream w_name;
                w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
                imshow("crop"+std::to_string(count), croppedImage);
                imwrite(w_name.str(), croppedImage);
                count++;
            }
        }
        
        waitKey();      //wait key to crop image by image in whole sheet
    }
    
    //Draw lines and crop stave segments
    /*int count = 0;
    
    for (int i=0; i<positions.size(); i++) {
        cout << "STAVE SEGMENT(GreenLine) POS and VAL : "<< i <<" : "<<positions[i] << endl;
        line(whole_image, Point(0, positions[i]), Point(whole_image.cols, positions[i]), Scalar( 0, 255, 0), 2, 0, 0);
        
        if (i==1) {
            //Crop first image
            cv::Mat image(wh_gsrc);
            cv::Rect myROI(0,0,wh_gsrc.cols,positions[i]);        //(x, y, width, height)
            croppedImage = image(myROI);
            std::ostringstream w_name;
            w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
            imshow("crop"+std::to_string(count), croppedImage);
            imwrite(w_name.str(), croppedImage);
            count++;
        }
        if (i==positions.size()){
            //Crop last image
            cv::Mat image(wh_gsrc);
            cv::Rect myROI(0,positions[i],wh_gsrc.cols,wh_gsrc.rows-positions[i]);        //(x, y, width, height)
            croppedImage = image(myROI);
            std::ostringstream w_name;
            w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
            imshow("crop"+std::to_string(count), croppedImage);
            imwrite(w_name.str(), croppedImage);
            count++;
        } else if (i>1) {
            if(i%2 != 0){
                continue;
            } else{
                //Crop middle images
                cv::Mat image(wh_gsrc);
                cv::Rect myROI(0,positions[i],wh_gsrc.cols,positions[i+1]-positions[i]);        //(x, y, width, height)
                croppedImage = image(myROI);
                std::ostringstream w_name;
                w_name << "/Users/allen/IOSProjects/MusicPal/Inputs/" << count << ".png";
                imshow("crop"+std::to_string(count), croppedImage);
                imwrite(w_name.str(), croppedImage);
                count++;
            }
        }
        
        //waitKey();      //wait key to crop image by image in whole sheet
    }*/

    
    imshow("Segmented line drawn image", whole_image);            //display segmented marked down imagek
    imwrite("/Users/allen/IOSProjects/MusicPal/InputImages/segmented_whole.png", whole_image);

    waitKey();
        
    
    
    //__--------------------------------LOADER ENDED---------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------
    
    //c_src = imread(filename, CV_LOAD_IMAGE_COLOR);
    
    //int z=606;
    std::stringstream ss;

    for (int z=0; z<count; z++) {
        printf("CROPPING(SEGMENTED) IMG NUMBER = %d\n",z);
        ss.str("");
        ss << "/Users/allen/IOSProjects/MusicPal/Inputs/" << z << ".png";
        string imageName  = ss.str();;
        printf("Segmenting Image No(Image Name): %s\n", imageName.c_str());

    if( argc > 1)
    {
        imageName = argv[1];
    }
    c_src = imread(imageName.c_str(), IMREAD_COLOR); // Read the file
    
    if(c_src.empty()) {
        help();
        cout << "can not open " << filename << endl;
        return -1;
    }
    
    cvtColor(c_src, g_src, CV_RGB2GRAY);
    
    Canny(g_src, edges, 50, 200, 3);
    imshow("Canny edges", edges);
    
    double angle = find_rotation_angle(edges, c_src);
    imshow("with rotation cal lines", c_src);
    
    std::cout << "Rotating image by " << abs(angle) << " degrees ";
    if(angle > 0) { std::cout << "counter-"; }
    std::cout << "clockwise."  << std::endl;
    
    // Fix the rotation of the music. Guarantee the staff is horizontal.
    rotate(c_src, angle, c_src);
    rotate(g_src, angle, g_src);
    
    threshold(g_src, bw_src, 100, 255, CV_THRESH_OTSU);//|CV_THRESH_BINARY);
    imshow("B&W", bw_src);
    imwrite( "/Users/allen/IOSProjects/MusicPal/bw.jpg", bw_src );
    vector<int> horiz_proj;
    vector<int> vert_proj;
    
    horiz_projection(bw_src, horiz_proj);
    //vert_projection(bw_src, vert_proj);
    
    vector<int> staff_positions; //staff line 1-5 y-axis values
    
    int max = *std::max_element(horiz_proj.begin(), horiz_proj.end());
    printf("Maximam horizontal projection %d \n",max);
    printf("Maximam horizontal projection by 8 - %f \n",max/8.0);
    
    for(int i = 0; i < horiz_proj.size(); i++) {            //here i is the y-axis values
        if(horiz_proj[i] > max/8.0) {                       //Default division value is 8.0, depending on staff line width
            staff_positions.push_back(i);
            remove_staff(bw_src, i);
            //remove_staff2(c_src, bw_src, i);
        }
    }
    imshow("2_No Staff", bw_src);
    imwrite("/Users/allen/IOSProjects/MusicPal/noStaff.jpg", bw_src);
    
    //get average pixel count between two lines
    int line_mid_px_count=0;
    for(int i = staff_positions[0]; i < staff_positions[staff_positions.size()-1]; i++) {
        if(horiz_proj[i] < max/8.0) {
            line_mid_px_count++;
        }
    }
    int non_line_avg_pxls = line_mid_px_count/8.0;
    printf("Average non line pixels: %d \n",non_line_avg_pxls);

    /*
    // Set up the detector with default parameters.
    SimpleBlobDetector detector;
    // Detect blobs.
    std::vector<KeyPoint> keypoints;
    detector.detect( bw_src, keypoints);
    
    // Draw detected blobs as red circles.
    // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
    Mat im_with_keypoints;
    drawKeypoints( bw_src, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
    
    // Show blobs
    imshow("keypoints", im_with_keypoints );
    waitKey(0);
    */
    for(int i = 0; i < staff_positions.size(); i++) {
        cout << "Staff position :  "<< i << "->" << staff_positions[i] << endl;
    }
    
    //-----------------------Vertical projection to src image----------------------
    vert_projection(bw_src, vert_proj);
    vector<int> v_p;
    for (int i=0; i<vert_proj.size(); i++) {
        //cout << vert_proj[i] << endl;
        if(vert_proj[i]>0 && vert_proj[i-1]==0){
            v_p.push_back(i-1);
        }
    }
    
    for (int i=0; i<v_p.size(); i++) {
        line(c_src, Point(v_p[i], 0), Point(v_p[i], c_src.rows), Scalar( 0, 0, 255), 2, 0, 0);
    }
    imshow("Vertical segments", c_src);
    //-----------------------Vertical END------------------------------------------
    
    vector<int> con_end_position;
    con_end_position = conseccount(staff_positions);
    
    vector<Rect> bboxes;
    Mat bw_temp = bw_src.clone();
    find_contours(thresh, bw_temp, bboxes, v_p);
    
    cv::HOGDescriptor hog;
    vector<float> descriptorsValues;
    vector<Point> locations;
    
    int i = 7;
    Mat img;
    bw_src(Rect(bboxes[i].x, bboxes[i].y, bboxes[i].width, bboxes[i].height)).copyTo(img);//x,y, width,height
    //imshow("bw_srctoimg", img);
    
    //hog.compute(img , descriptorsValues, Size(0,0), Size(0,0), locations);
    waitKey();
    
    cout << "HOG descriptor size is " << hog.getDescriptorSize() << endl;
    cout << "img dimensions: " << img.cols << " width x " << img.rows << " height" << endl;
    cout << "Found " << descriptorsValues.size() << " descriptor values" << endl;
    cout << "Nr of locations specified : " << locations.size() << endl;
    
    get_SortedBoxes(bboxes);
    
    vector<int> upper_side;
    vector<int> lower_side;
    
    int s = 0;
    //Save the small notes
    for(int i = 0; i<bboxes.size(); i++) {
        //ignore the boxes below 100
        if(bboxes[i].width * bboxes[i].height < 10) {
            continue;
        }
        
        cout << "BbOX X axis : " << bboxes[i].x << endl;
        imshow("small", bw_src(Range(bboxes[i].y, bboxes[i].y+bboxes[i].height), Range(bboxes[i].x, bboxes[i].x + bboxes[i].width)));
        
        setPitch(bboxes[i].y, (bboxes[i].y+bboxes[i].height), upper_side, lower_side, staff_positions, con_end_position, non_line_avg_pxls);
        cout << "Upper Pitch Level :  "<< upper_side[s] << endl;
        //cout << "Lower Pitch Level :  "<< lower_side[s] << endl;
        
        //Write files to disk
        std::ostringstream name;
        if (upper_side[s]<10 && lower_side[s]<10) {
            name << "/Users/allen/IOSProjects/MusicPal/FinalOutputs/"<<z<<"_" << s <<"_"<<"0"<<lower_side[s]<<"_"<<"0"<<upper_side[s] << ".png";
        } else if (upper_side[s]<10) {
            name << "/Users/allen/IOSProjects/MusicPal/FinalOutputs/"<<z<<"_" << s <<"_"<<lower_side[s]<<"_"<<"0"<<upper_side[s] << ".png";
        } else if (lower_side[s]<10) {
            name << "/Users/allen/IOSProjects/MusicPal/FinalOutputs/"<<z<<"_" << s <<"_"<<"0"<<lower_side[s]<<"_"<<upper_side[s] << ".png";
        } else {
            name << "/Users/allen/IOSProjects/MusicPal/FinalOutputs/"<<z<<"_" << s <<"_"<<lower_side[s]<<"_"<<upper_side[s] << ".png";
        }
        imwrite(name.str(), bw_src(Range(bboxes[i].y, bboxes[i].y+bboxes[i].height), Range(bboxes[i].x, bboxes[i].x + bboxes[i].width)));
        s++;
        
        waitKey();    //wait image by image segmentation
    }
    
    for(int i = 0; i < upper_side.size(); i++) {        //lower_side.size()
        cout << "Pitch U-"<< upper_side[i] <<" L-"<<lower_side[i] << endl;
        //cout << "Pitch U-"<< upper_side[i] <<" L-"<<lower_side[i] << endl;

    }
    
    
    //waitKey();        //Wait inside for loop row image to row image
        
    }
    
    waitKey();          //wait until press key to finish the program

    
    return 0;

}


