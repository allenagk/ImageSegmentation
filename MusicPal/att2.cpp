#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp" // For HoG.
#include "opencv2/features2d.hpp"
#include "opencv2/opencv.hpp"
#include "baseapi.h"

using namespace cv;
using namespace std;

RNG rng(12345);

int main ( int argc, char const *argv[] ) {

  int i, j;

  if( argc != 2 )
   {
    cout << "Please enter image name" << '\n';
    return -1;
   }

  Mat sourceImage = imread( argv[1], CV_LOAD_IMAGE_GRAYSCALE );
  Mat binaryImage;
  if ( !sourceImage.data )
   {
    cout << "Please enter a non-empty image" << '\n';
    return -1;
   }

  threshold( sourceImage, binaryImage, 0, 255, THRESH_OTSU );
  imshow ( "Binarised", binaryImage );
  waitKey(0);

  Mat eroded;

  Mat element1 = getStructuringElement( MORPH_CROSS, Size(5, 5), Point(-1, -1) );
  //Mat element2 = getStructuringElement( MORPH_RECT, Size(3, 1), Point(-1, -1) );

  erode( binaryImage, eroded, element1, cv::Point(-1, -1), 5 );
  //erode( eroded, eroded, element2, cv::Point(-1, -1), 5 );
  imshow( "Eroded", eroded );
  waitKey(0);

  vector<vector<Point> > contours;
  vector<Vec4i> hierarchy;
  findContours( eroded, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE );

  Mat drawing = Mat::zeros( eroded.size(), CV_8UC3 );

  for( int i = 0; i < contours.size(); i++ )
     {
       Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
       drawContours( drawing, contours, i, color, 2, 8, hierarchy, 0, Point() );
     }

  namedWindow( "Contours", CV_WINDOW_AUTOSIZE );
  imshow( "Contours", drawing );
  waitKey(0);

  vector<vector<Point> > contours_poly( contours.size() );
  vector<Rect> boundRect( contours.size() );
  //vector<Point2f>center( contours.size() );
  //vector<float>radius( contours.size() );

  for( int i = 0; i < contours.size(); i++ )
     { approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
       boundRect[i] = boundingRect( Mat(contours_poly[i]) );
       //minEnclosingCircle( (Mat)contours_poly[i], center[i], radius[i] );
     }

  drawing = Mat::zeros( eroded.size(), CV_8UC3 );
  for( int i = 0; i < contours.size(); i++ )
    {
          Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
          drawContours( drawing, contours_poly, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
          rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );
          //circle( drawing, center[i], (int)radius[i], color, 2, 8, 0 );
    }

 /// Show in a window
  namedWindow( "semifinal", 0 );
  resizeWindow("semifinal", 500,500);
  imshow( "semifinal", drawing );
  waitKey(0);

  for( int i = 0; i < contours.size(); i++ )
    {
          Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
          rectangle( sourceImage, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0 );
          //circle( drawing, center[i], (int)radius[i], color, 2, 8, 0 );
    }

    imwrite( "attempt2.jpg", sourceImage );

    namedWindow( "final", 0 );
    resizeWindow("final", 500,500);
    imshow( "final", sourceImage );
    waitKey(0);

  return 0;
 }
