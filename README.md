This module contains Image preprocessing techniques (Contrast Equalization, Smoothing, Thresholding, Skew Correction) 
and Image Segmentation part( removing stave lines, symbol segmentation, scaling, detecting the pitch) built on MacOS.

main.cpp file is on the /MusicPal/main.cpp directory.
Application was built on Xcode framework and UI was built by Qt framework.